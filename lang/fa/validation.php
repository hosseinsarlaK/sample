<?php

return [
    'required' => ':attributeالزامی میباشد. ',
    'attributes' => [
        'name' => 'نام ویدیو',
        'length' => 'مدت زمان ',
        'slug' => 'نام یکتا',
        'url' => 'آدرس ویدیو',
        'thumbnail' =>' تصویربندانگشتی ',
    ]
];
